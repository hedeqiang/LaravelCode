<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Requirement
    PHP >= 7.0
    node >6


## Installation
    git clone https://github.com/hedeqiang/LaravelCode.git
    
    cd LaravelCode 
    
    cp .env.example .env #配置数据库连接、邮件服务、GitHub client_id client_secret等
    
    composer install
    
    php artisan migrate
    
    npm install
    
    npm run dev or  npm run watch-poll
    
    
## 项目中所使用的扩展包

* [overtrue/laravel-lang](https://github.com/overtrue/laravel-lang)
* [overtrue/laravel-socialite](https://github.com/overtrue/laravel-socialite)
* [overtrue/laravel-emoji](https://github.com/overtrue/laravel-emoji)
* [OneSignal/emoji-picker](https://github.com/OneSignal/emoji-picker)
* [sparksuite/simplemde-markdown-editor](https://github.com/sparksuite/simplemde-markdown-editor)
* [Rovak/InlineAttachment](https://github.com/Rovak/InlineAttachment)
* [erusev/parsedown](https://github.com/erusev/parsedown)
* [highlightjs/highlight.js](https://github.com/highlightjs/highlight.js)
* [Laravel Visits](https://github.com/awssat/laravel-visits)
* [JellyBool/translug](https://github.com/JellyBool/translug)
* [ElfSundae/laravel-gravatar](https://github.com/ElfSundae/laravel-gravatar)
* [torann/geoip ](https://github.com/Torann/laravel-geoip)


## 遇到的坑
> Laravel5.5 所采用的 bootstrap 版本为 3 ，但是我升级为bootstrap4，然后暂时没发现其他问题，唯独分页有问题
分页样式全无

bootstrap4分页样式为以下：
```
<nav aria-label="Page navigation example">
  <ul class="pagination">
    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
    <li class="page-item"><a class="page-link" href="#">1</a></li>
    <li class="page-item"><a class="page-link" href="#">2</a></li>
    <li class="page-item"><a class="page-link" href="#">3</a></li>
    <li class="page-item"><a class="page-link" href="#">Next</a></li>
  </ul>
</nav>
```
而bootstrap 3 则为：
```
<nav aria-label="Page navigation">
  <ul class="pagination">
    <li>
      <a href="#" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    <li><a href="#">1</a></li>
    <li><a href="#">2</a></li>
    <li><a href="#">3</a></li>
    <li><a href="#">4</a></li>
    <li><a href="#">5</a></li>
    <li>
      <a href="#" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
</nav>
```
不难看出，bootstrap 4 为每一个li > a添加了单独的样式，所以：

#### 解决方法如下：
```
php artisan vendor:publish --tag=laravel-pagination

```
以上命令会在 `resource/views/vendor/pagination` 目录中生成分页模板

之后在页面模板中使用：
```
{!! $posts->links('vendor.pagination.bootstrap-4') !!}

```

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

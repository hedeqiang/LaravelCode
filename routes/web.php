<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','HomeController@root')->name('root');

Route::get('/reprinted', function () {
    return view('reprinted');
});

Route::get('/sites','LinkController@index')->name('links.index');
Route::get('/links/create','LinkController@create')->name('links.create');
Route::post('/links/store','LinkController@store')->name('links.store');

Route::get('users_json','UsersController@usersJson')->name('users.json');


Auth::routes();

Route::group(['middleware' => 'auth'], function() {
    Route::get('/email_verify_notice', 'HomeController@emailVerifyNotice')
        ->name('email_verify_notice');

    Route::get('/email_verification/send', 'EmailVerificationController@send')->name('email_verification.send');
    Route::get('/email_verification/verify', 'EmailVerificationController@verify')->name('email_verification.verify');
    Route::get('/about','HomeController@about')->name('about');

    Route::resource('notifications', 'NotificationsController', ['only' => ['index']]);

    Route::group(['middleware' => 'email_verified'], function() {
        Route::resource('users','UsersController');
        Route::any('users/upload','UsersController@upload')->name('users.upload');

        Route::resource('posts','PostsController',['only' => ['index', 'create', 'store', 'update', 'edit', 'destroy']]);
        Route::post('upload_image', 'PostsController@uploadImage')->name('posts.upload_image');
        Route::resource('replies', 'RepliesController', ['only' => ['store', 'destroy']]);
        Route::get('users/{user}/follow', 'UsersController@follow')->name('users.follow');
        Route::get('users/{user}/unfollow', 'UsersController@unfollow')->name('users.unfollow');

        Route::post('tweets','TweetsController@store')->name('tweets.store');

    });

});
Route::resource('tweets','TweetsController',['only' => ['index','show']]);

Route::get('posts/{post}/{slug?}', 'PostsController@show')->name('posts.show');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/oauth/github', 'AuthController@redirectToProvider')->name('oauth.github');
Route::get('/oauth/github/callback', 'AuthController@handleProviderCallback')->name('oauth.github.callback');

Route::get('/oauth/qq', 'AuthController@qqRedirectToProvider')->name('oauth.qq');
Route::get('/oauth/qq/callback', 'AuthController@qqHandleProviderCallback')->name('oauth.qq.callback');

Route::get('oauth/perfect', 'AuthController@perfect')->name('oauth.perfect');
Route::post('oauth/perfect/store','AuthController@store')->name('auth.perfect.store');



//后台相关
Route::get('/admin/login','Admin\Auth\LoginController@showLoginForm');
Route::post('/admin/login','Admin\Auth\LoginController@login')->name('admin.login');
Route::post('/admin/logout','Admin\Auth\LoginController@logout')->name('admin.logout');

Route::get('/admin/index',function (){
    return view('admin.pages.index');
})->name('admin.index');

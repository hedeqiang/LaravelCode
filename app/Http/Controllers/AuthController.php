<?php

namespace App\Http\Controllers;

use App\Http\Requests\PerfectRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Overtrue\LaravelSocialite\Socialite;

class AuthController extends Controller
{
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('github')->redirect();
    }

    /**
     * Redirect the user to the QQ authentication page.
     * @return mixed
     */
    public function qqRedirectToProvider()
    {
        return Socialite::driver('qq')->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback()
    {
        $user = Socialite::driver('github')->user();
        if(!User::where('email',$user->email)->first()){
            User::create([
                'name' => $user->name,
                'email' => $user->email,
                'password' => bcrypt('password'),
                'remember_token' => bcrypt('password'),
                'avatar' => $user->avatar,
            ]);
        }

        $userInstance = User::where('name',$user->name)->firstOrFail();
        Auth::login($userInstance);

        return redirect('/');
    }

    /**
     * Obtain the user information from QQ.
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function qqHandleProviderCallback()
    {
        $user = Socialite::driver('qq')->user();
        if(!User::where('name',$user->name)->first()){
            return view('auth.perfect',compact('user'));
        }
//
        $userInstance = User::where('name',$user->name)->firstOrFail();
        Auth::login($userInstance);
        return redirect('/');
    }

    public function perfect()
    {
        return view('auth.perfect');
    }

    public function store(PerfectRequest $request)
    {
        if(!User::where('email',$request->input('email'))->first()){
            User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
                'remember_token' => bcrypt($request->input('password')),
                'avatar' => $request->input('avatar'),
            ]);
        }

        $userInstance = User::where('email',$request->input('email'))->firstOrFail();
        Auth::login($userInstance);

        return redirect('/');
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['index']]);
    }


     public function index()
     {
         $links = Link::all();
         return view('links.index',compact('links'));
     }

     public function create(Link $link)
     {
         return view('links.create_and_edit',compact('link'));
     }

     public function store(Request $request, Link $link)
     {
         $this->validate($request, [
             'title' => 'required',
             'link' => 'required',
         ]);

         $link->fill($request->all());
         $link->save();
         return redirect('/sites')->with('message', '发布成功');
     }
}

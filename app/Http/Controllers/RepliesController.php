<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ReplyRequest;
use Auth;

class RepliesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @param ReplyRequest $request
     * @param Reply $reply
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(ReplyRequest $request, Reply $reply)
    {
        //dd($request->all());
        $content = clean($request->get('content'));
        if (empty($content)) {
            return redirect()->back()->with('error', '非法！');
        }


        $reply->content = $request->input('content');
        $reply->user_id = Auth::id();
        $reply->post_id = $request->post_id;
        $reply->save();
        return redirect()->to($reply->post->link())->with('sucess', '回复创建成功！');
    }

    /**
     * @param Reply $reply
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Exception
     */
    public function destroy(Reply $reply)
    {
        //$this->authorize('destroy', $reply);
        $reply->delete();
        return redirect()->to($reply->post->link())->with('success', '成功删除回复！');
    }
}

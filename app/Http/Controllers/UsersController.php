<?php

namespace App\Http\Controllers;

use App\Handlers\ImageUploadHandler;
use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function show(User $user)
    {
        return view('users.show', compact('user'));
    }

    public function upload(Request $request,ImageUploadHandler $uploader,User $user)
    {
        if ($request->avatar) {
            $result = $uploader->save($request->avatar, 'avatars', $user->id);
            if ($result) {
                $data['avatar'] = $result['path'];
            }
        }
        $user->update($data);
        return redirect()->route('users.show', $user->id)->with('success', '头像修改成功！');
    }

    //关注
    public function follow(Request $request, User $user)
    {
        if($request->user()->canFollow($user)) {
            $request->user()->following()->attach($user);
        }
        return redirect()->back();
    }

    //取消
    public function unFollow(Request $request, User $user)
    {
        if($request->user()->canUnFollow($user)) {
            $request->user()->following()->detach($user);
        }
        return redirect()->back();
    }


    public function usersJson(Request $request)
    {
        $name = $request->q;
        $users  =User::where('name','like',$name."%")->pluck('name')->toArray();
        return response()->json($users);
    }

}

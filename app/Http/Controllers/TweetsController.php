<?php

namespace App\Http\Controllers;

use App\Http\Requests\TweetRequest;
use App\Models\Tweet;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TweetsController extends Controller
{
    protected $fillable  = ['user_id','body'];

    public function __construct()
    {
        $this->middleware('auth')->only('store');
    }

    public function show(Request $request,Tweet $tweet)
    {
        $tweet->visits()->increment();
        dd($tweet);
    }

    public function index()
    {
        $tweets = Tweet::orderBy('id','desc')->with('user')-> paginate(15);
        return view('tweets.index',compact('tweets'));
    }

    public function store(TweetRequest $request,Tweet $tweet)
    {
        $tweet->fill($request->all());
        $tweet->user_id = Auth::id();
        $tweet->save();
        return redirect()->route('tweets.index')->with('success','发布成功');

    }
}

<?php

namespace App\Observers;

use App\Models\Post;
use App\Models\Reply;
use App\Notifications\PostReplied;

class ReplyObserver
{
    public function created(Reply $reply)
    {
        $post = $reply->post;
        $post->increment('reply_count', 1);

        // 通知作者话题被回复了
        $post->user->notify(new PostReplied($reply));
    }

//    public function creating(Reply $reply)
//    {
//        $reply->content = clean($reply->content, 'user_post_body');
//    }
}

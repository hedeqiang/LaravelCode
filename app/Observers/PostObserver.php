<?php

namespace App\Observers;

use App\Models\Post;

class PostObserver
{
    public function saving(Post $post)
    {
        // XSS 过滤
        //$post->body = clean($post->body, 'user_post_body');

        // 生成话题摘录
        $post->excerpt = make_excerpt($post->body);

        // 如 slug 字段无内容，即使用翻译器对 title 进行翻译
        if ( ! $post->slug) {
            $post->slug = app('translug')->translug($post->title);
        }
    }
}

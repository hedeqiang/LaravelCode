<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewCountAndReplyCountToTwetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tweets', function (Blueprint $table) {
            $table->integer('reply_count')->unsigned()->default(0)->comment('回复数量')->after('body');
            $table->integer('view_count')->unsigned()->default(0)->comment('查看总数')->after('body');
            $table->integer('like_count')->unsigned()->default(0)->comment('点赞总数')->after('body');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tweets', function (Blueprint $table) {
            $table->dropColumn('reply_count');
            $table->dropColumn('view_count');
            $table->dropColumn('like_count');
        });
    }
}

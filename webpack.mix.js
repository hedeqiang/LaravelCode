let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css')
    .copyDirectory('resources/assets/emoji/js', 'public/js')
    .copyDirectory('resources/assets/emoji/css', 'public/css')
    .copyDirectory('resources/assets/emoji/img', 'public/images/emoji')
    .copyDirectory('resources/assets/inline-attachment', 'public/js')
    .copyDirectory('resources/assets/simplemd/js', 'public/js')
    .copyDirectory('resources/assets/simplemd/css', 'public/css')
    .copyDirectory('resources/assets/hightlight', 'public/css')
    .copyDirectory('resources/assets/toTop/jquery.toTop.min.js', 'public/js')
    .copyDirectory('resources/assets/toTop/jquery-1.11.0.min.js', 'public/js')
    .copyDirectory('resources/assets/at/js/jquery.atwho.min.js', 'public/js')
    .copyDirectory('resources/assets/at/css/jquery.atwho.min.css', 'public/css')
    .copyDirectory('resources/assets/at/js/jquery.caret.min.js', 'public/js')
    ;

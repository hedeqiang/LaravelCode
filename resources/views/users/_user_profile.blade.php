<div class="jumbotron UserCard Hero UserHero">
    <div class="container">
        <div class="UserCard-profile">
                <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                    <img src="{{ $user->avatar }}"
                         class="img-responsive rounded-circle" width="96px" height="96px">
                </span>
            <div class="text-left" style="">
                <h2 class="UserCard-identity">

                    <div class="UserCard-avatar"></div>
                    <span class="username">{{ $user->name }}</span>

                </h2>
                <ul class="UserCard-badges badges">
                    <li class="item-group1">
                                <span class="Badge Badge--group--1 " title=""
                                      style="background-color: rgb(183, 42, 42);" data-original-title="创建者">
                                    <i class="icon fa fa-wrench Badge-icon"></i>
                                </span>
                    </li>
                </ul>
                <ul class="UserCard-info">
                    <li class="item-bio">
                        <div class="UserBio ">
                            <div class="UserBio-content"></div>
                        </div>
                    </li>
                    <li class="item-uid">UID： {{ $user->id }}</li>
                    <li class="item-lastSeen">
                                <span class="UserCard-lastSeen">
                                    <i class="icon fa fa-clock-o "></i> {{ $user->created_at->diffForHumans() }}
                                </span>
                    </li>
                    <li class="item-joined">加入于 {{ $user->created_at->formatLocalized('%Y年-%m月-%d日') }}</li>
                    <li class="item-fa-github-0 social-button">
                                <span style="" class="Badge Badge--social social-icon-0 " title=""
                                      data-original-title="Git">
                                    <i class="icon fa fa-github Badge-icon"></i>
                                </span>
                    </li>
                    @if(auth()->user()->isNot($user))
                        @if(auth()->user()->isFollowing($user))
                            <a href="{{ route('users.unfollow', $user) }}" class="btn btn-danger pull-right">取消关注</a>
                        @else
                            <a href="{{ route('users.follow', $user) }}" class="btn btn-success pull-right">关注我</a>
                        @endif
                    @endif
                </ul>
            </div>

        </div>
    </div>
</div>
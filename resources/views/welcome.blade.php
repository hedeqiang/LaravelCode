@extends('layouts.app')
@section('title', 'LaravelCode|PHP')
@section('description', 'LaravelCode|PHP 贺德强个人博客')
@section('keywords', 'LaravelCode,php,laravel,贺德强,hedeqiang,贺德强个人博客')
@section('content')

    <!--<div class="jumbotron jumbotron-fluid" style="background-color: lightseagreen;">
          <div class="container">
            <h1 class="display-3">Fluid jumbotron</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
          </div>
        </div>-->
    <div class="main">
        <div class="row">
            <div class="col-md-9">
                <div class="{{ route_class() }}">
                    @foreach ($posts as $post)
                    <div class="container">
                        <div>
                            <ul class="list-group row topic-list" >
                                <li class="list-group-item ">
                                    <a class="reply_count_area hidden-xs pull-right"
                                       href="{{ $post->link() }}">
                                        <div class="count_set">
                                            <span class="count_of_replies" title="回复数">
                                               {{ $post->reply_count }}
                                             </span>
                                            <span class="count_seperator">/</span>
                                            <span class="count_of_visits" title="查看数">
                                               {{ $post->visits()->count() }}
                                             </span>
                                            <span class="count_seperator">|</span>
                                            <abbr title="{{ $post->created_at->toDateTimeString() }}" class="timeago">{{ $post->created_at->diffForHumans() }}</abbr>
                                        </div>
                                    </a>
                                    <div class="avatar pull-left">

                                        <a href="{{ $post->user->profileLink }}" title="{{ $post->user->name }}">
                                            <img class="media-object  avatar avatar-middle" alt="{{ $post->user->name }}"
                                                 src="{{ $post->user->avatar }}">
                                        </a>
                                    </div>
                                    <div class="infos">
                                        <div class="media-heading">
                                            {{--<span class="hidden-xs label label-warning">置顶</span>--}}
                                            <a href="{{ $post->link() }}">
                                                {{ $post->title }}
                                            </a>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>

                    @endforeach

                </div>



                <div class="pull-right root-page">
                    {!! $posts->links('vendor.pagination.bootstrap-4') !!}
                </div>


            </div>

            @include('layouts.right',['posts' => visits(\App\Models\Post::class)->top(5)])

        </div>
    </div>

@stop

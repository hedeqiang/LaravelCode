@extends('layouts.app')
@section('title', '转载文章')
@section('description', 'LaravelCode|PHP 贺德强个人博客')
@section('keywords', 'LaravelCode,php,laravel,贺德强,hedeqiang,贺德强个人博客,转载')
@section('content')

    <!--<div class="jumbotron jumbotron-fluid" style="background-color: lightseagreen;">
          <div class="container">
            <h1 class="display-3">Fluid jumbotron</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
          </div>
        </div>-->
    <div class="main">
        <div class="row">
            <div class="col-md-9">
                <div class="article">
                    {{--@foreach ($topics as $topic)--}}
                    <div class="card" style="margin-bottom: 0.5rem;">
                        <div class="card-body">
                            <div>
                                <p class="card-text">With supporting text below as a natural lead-in to additional
                                    content.</p>
                                <span class="title-date">
							    			<i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                    2018-09-10 10:23:35
                                    {{--                                            {{ $topic->created_at->diffForHumans() }}--}}
							    		</span>
                                <span class="author">
							    			<i class="fa fa-user-circle"></i>
                                    hedeqiang
                                    {{--                                            {{ $topic->user->name }}--}}
							    		</span>
                            </div>


                            <div class="detail pull-right">
                                <a href="{{ url('/detail') }}" title="" class="btn btn-outline-success">
                                    查看详情
                                </a>
                                {{--<a href="/detail/{{$topic->id}}" class="btn btn-primary">--}}
                                {{--阅读全文--}}
                                {{--</a>--}}
                            </div>

                        </div>
                    </div>
                    {{--@endforeach--}}
                </div>

                <nav aria-label="Page navigation example">
                    {{--                        {!! $topics->render() !!}--}}
                </nav>

            </div>

            @include('layouts._right-reprinted')

        </div>
    </div>

@stop

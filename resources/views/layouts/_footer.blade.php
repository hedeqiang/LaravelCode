<footer id="footer" class="footer">
        <div class="alert alert-dark alert-footer " role="alert">
            <p class="mb-0 text-center">
                &copy; Company 2018-<a href="{{ url('/') }}">贺德强</a> 版权所有
                <span style="color: #e27575;font-size: 14px;"></span>
                <a class="alert-link" href="http://www.miitbeian.gov.cn">冀ICP备17012521号-2
                </a>

            <span class="mb-0 pull-right">
                <a class="footer-contact" href="{{ url('/sites#web') }}">推荐资源</a>
                <a class="footer-contact" href="{{ url('/sites#blog') }}">友情链接</a>
                来自: {{$location}}</span>
            </p>
        </div>
</footer>



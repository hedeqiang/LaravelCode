<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light bg-light">
        <a class="navbar-brand" href="{{ url('/') }}">LaravelCode</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">原创 <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('/reprinted') }}">转载</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">分类</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('tweets.index') }}">动弹</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="http://flarum.laravelcode.cn">后宫</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ url('about') }}">关于我</a>
                </li>
                {{--<li class="nav-item dropdown">--}}
                {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
                {{--Dropdown--}}
                {{--</a>--}}
                {{--<div class="dropdown-menu" aria-labelledby="navbarDropdown">--}}
                {{--<a class="dropdown-item" href="#">Action</a>--}}
                {{--<a class="dropdown-item" href="#">Another action</a>--}}
                {{--<div class="dropdown-divider"></div>--}}
                {{--<a class="dropdown-item" href="#">Something else here</a>--}}
                {{--</div>--}}
                {{--</li>--}}
            </ul>
            <form class="form-inline my-2 my-lg-0">
                <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            {{--<a class="btn btn-outline-success mr-sm-2 my-2 my-sm-1" href="{{ url('login') }}" role="button">登录</a>--}}
            {{--<a class="btn btn-outline-success my-2 my-sm-0" href="{{ url('register') }}" role="button">注册</a>--}}
            <!-- 登录注册链接开始 -->
            </form>
            @guest
                <a class="btn btn-outline-success mr-sm-2 my-2 my-sm-1"
                                        href="{{ route('login') }}">登录</a>
                <a class="btn btn-outline-success mr-sm-2 my-2 my-sm-1"
                                        href="{{ route('register') }}">注册</a>
            @else
                <li>
                    <a href="{{ route('notifications.index') }}" class="fa fa-bell" style="margin-left: 0.7rem; margin-right: 1rem; color: {{ Auth::user()->notification_count > 0 ? 'red' : '#2ab27b' }} ;">
                            {{--<span class="badge badge-{{ Auth::user()->notification_count > 0 ? 'hint' : 'fade' }} " style=" display: inline-flex" title="消息提醒">--}}
                                {{--{{ Auth::user()->notification_count }}--}}
                            {{--</span>--}}
                    </a>
                </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <span class="user-avatar pull-left" style="margin-right:8px; margin-top:-5px;">
                                <img src="{{ Auth::user()->avatar }}"
                                     class="img-responsive rounded-circle" width="30px" height="30px">
                            </span>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        <li>
                            <a class="dropdown-item" href="{{ url('/users').'/'. Auth::id() }}">个人资料</a>
                        </li>
                        <li class="dropdown-item">
                            <a href="{{ route('logout') }}"
                               onclick="event.preventDefault();
                                             document.getElementById('logout-form').submit();">
                                退出登录
                            </a>
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                {{ csrf_field() }}
                            </form>
                        </li>
                    </ul>
                </li>
            @endguest
        <!-- 登录注册链接结束 -->

        </div>
    </nav>
</div>


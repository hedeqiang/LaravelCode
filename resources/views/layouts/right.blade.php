<div class="col-md-3">
                <div class="card text-white bg-success mb-3" style="max-width: 25rem; opacity: 0.8;">
                    <div class="card-header">网站公告</div>
                    <div class="card-body">

                        <p class="card-text saying">
                            LaravelCode 正式上线啦！
                        </p>
                    </div>
                </div>


                <div class="list-group" style="max-width: 25rem;">
                    <a href="#" class="list-group-item list-group-item-action active">
                        热门排行
                    </a>

                   @foreach($posts as $post)
                        <a href="{{ $post->link() }}" class="list-group-item list-group-item-action">{{ $post->title }}</a>

                    @endforeach

                </div>



                @include('posts.categories')
            </div>
@extends('layouts.app')
@section('title', '关于我')
@section('description', 'LaravelCode|PHP 贺德强个人博客')
@section('keywords', 'LaravelCode,php,laravel,贺德强,hedeqiang,贺德强个人博客')

@section('content')
    <div class="jumbotron">
        <h1 class="display-4">About Me</h1>
        <p class="lead font-weight-bold ">姓名： 贺德强</p>
        <p class="lead font-weight-bold ">性别： 男</p>
        <p class="lead font-weight-bold ">年龄： 90后</p>
        <p class="lead font-weight-bold ">邮箱： laravel_code@163.com</p>
        <p class="lead font-weight-bold ">QQ： 760963254</p>
        <p class="lead font-weight-bold ">WeChat： 同上</p>
        <p class="lead font-weight-bold ">Github： <a href="https://github.com/hedeqiang">Github</a></p>
        <hr class="my-4">
        <p>喜欢PHP，喜欢Laravel，喜欢开发，但就是什么都不会......尴尬不 {{ \Overtrue\LaravelEmoji\Emoji::shortnameToUnicode(':smile:') }}</p>
        <p class="lead">
            <a class="btn btn-primary btn-lg" href="{{ url('/') }}" role="button">Learn more</a>
        </p>
    </div>

@stop

@extends('layouts.app')
@section('title', '推荐站点')

@section('content')
    <div class="card w-100 site-index">
        <div class="card-body">
            <h5 class="card-title" id="web">推荐网站</h5>
            <hr class="hr-style">
            <div class="body row">
                @foreach($links as $link)
                    @if($link->is_site === 1)
                        <div class="col-md-2 site">
                            <a class="popover-with-html" target="_blank" href="{{ $link->link }}">
                                {{ $link->title }}
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>


    <div class="card w-100 site-index">
        <div class="card-body">
            <h5 class="card-title" id="blog">推荐博客</h5>
            <hr class="hr-style">
            <div class="body row">
                @foreach($links as $link)
                    @if($link->is_site === 0)
                        <div class="col-md-2 site">
                            <a class="popover-with-html" target="_blank" href="{{ $link->link }}">
                                {{ $link->title }}
                            </a>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>



@stop

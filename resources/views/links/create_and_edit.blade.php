@extends('layouts.app')
@section('title', '添加资源')

@section('content')
    {{--@include('users._user_profile')--}}
    <div class="container">
        {{--@include('layouts._left_users')--}}
        <div class=" ">

            <h2 class="text-center">
                <i class="glyphicon glyphicon-edit"></i>
                @if($link->id)
                    编辑资源
                @else
                    新建资源
                @endif
            </h2>

            <hr>

            @include('layouts._message')

            {{--@include('common.error')--}}

            @if($link->id)
                <form action="{{ route('links.update', $link->id) }}" method="POST" accept-charset="UTF-8">
                    <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="{{ route('links.store') }}" method="POST" accept-charset="UTF-8">
                            @endif

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <label for="title">请填写资源标题</label>
                                <input class="form-control" id="title" type="text" name="title"
                                       value="{{ old('title', $link->title ) }}" placeholder="请填写标题" required/>
                            </div>

                            <div class="form-group">
                                <label for="link">请填写资源链接</label>
                                <input name="link" id="link" class="form-control"
                                          placeholder="请填写资源链接" value="{{ old('link', $link->link ) }}" />
                            </div>

                            <div class="form-group">
                                <label for="is_site">请选择资源类型</label>
                                <select name="is_site" id="is_site" class="form-control">
                                    <option value="1">网站</option>
                                    <option value="0">博客</option>
                                </select>
                            </div>


                            <div class="well well-sm">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"
                                                                                    aria-hidden="true"></span> 保存
                                </button>
                            </div>
                        </form>
        </div>
    </div>

@stop

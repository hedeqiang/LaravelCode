@extends('layouts.app')
@section('title', '操作成功')

@section('content')
    <div class="card">
        <div class="card-header">
            操作成功
        </div>
        <div class="card-body text-center">
            <h5 class="card-title"></h5>
            <h1>{{ $msg }}</h1>
            <a class="btn btn-outline-success" href="{{ route('root') }}">返回首页</a>
        </div>
    </div>
@endsection
<div class="card text-white bg-info mb-3" style="max-width: 25rem; margin-top: 1rem;">
    <div class="card-header">
        <span>
            标签云
        </span>
    </div>
    <div class="card-body">
        <div class="tags">
            @foreach(\App\Models\Category::all() as $category)
                <span style="background-color: {{ $category->color }}" class="badge">{{ $category->name }}</span>

            @endforeach
        </div>

    </div>
</div>
@extends('layouts.app')
@section('title', '新建文章')

@section('styles')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/highlight.js/latest/styles/github.min.css">
@stop

@section('content')
    {{--@include('users._user_profile')--}}
    <div class="container">
        {{--@include('layouts._left_users')--}}
        <div class=" ">

            <h2 class="text-center">
                <i class="glyphicon glyphicon-edit"></i>
                @if($post->id)
                    编辑文章
                @else
                    新建文章
                @endif
            </h2>

            <hr>

            {{--@include('common.error')--}}

            @if($post->id)
                <form action="{{ route('posts.update', $post->id) }}" method="POST" accept-charset="UTF-8">
                    <input type="hidden" name="_method" value="PUT">
                    @else
                        <form action="{{ route('posts.store') }}" method="POST" accept-charset="UTF-8">
                            @endif

                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                            <div class="form-group">
                                <input class="form-control" type="text" name="title"
                                       value="{{ old('title', $post->title ) }}" placeholder="请填写标题" required/>
                            </div>

                            <div class="form-group">
                                <select class="form-control" name="category_id" required>
                                    <option value="" hidden disabled {{ $post->id ? '' : 'selected' }}>请选择分类</option>
                                    @foreach ($categories as $value)
                                        <option value="{{ $value->id }}" {{ $post->category_id == $value->id ? 'selected' : '' }}>{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <textarea name="body" class="form-control"
                                          id="simplemde">{{ old('body', $post->body ) }}</textarea>
                            </div>


                            <div class="well well-sm">
                                <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"
                                                                                    aria-hidden="true"></span> 保存
                                </button>
                            </div>
                        </form>
        </div>
    </div>

@stop

@section('scripts')
    <script src="{{asset('js/inline-attachment.js')}}"></script>
    <script src="{{asset('js/codemirror.inline-attachment.js')}}"></script>

    {{--<script src="https://cdn.jsdelivr.net/simplemde/latest/simplemde.min.js"></script>--}}
    <script src="{{ asset('js/simplemde.min.js') }}"></script>
    {{--<script src="https://cdn.jsdelivr.net/highlight.js/latest/highlight.min.js"></script>--}}

    <script>

        $(document).ready(function () {
            var simplemde = new SimpleMDE({
                spellChecker: false,
                autosave: {
                    enabled: true,
                    delay: 5000,
                    unique_id: "topic_content"
                },
                forceSync: true,
                tabSize: 4,
                toolbar: [
                    "bold", "italic", "heading", "|", "quote", "code", "table",
                    "horizontal-rule", "unordered-list", "ordered-list", "|",
                    "link", "image", "|", "side-by-side", 'fullscreen', "|",
                    {
                        name: "guide",
                        action: function customFunction(editor) {
                            var win = window.open('https://github.com/riku/Markdown-Syntax-CN/blob/master/syntax.md', '_blank');
                            if (win) {
                                //Browser has allowed it to be opened
                                win.focus();
                            } else {
                                //Browser has blocked it
                                alert('Please allow popups for this website');
                            }
                        },
                        className: "fa fa-info-circle",
                        title: "Markdown 语法！",
                    },
                    {
                        name: "publish",
                        action: function customFunction(editor) {
                            $('#topic-submit').click();
                        },
                        className: "fa fa-paper-plane",
                        title: "发布文章",
                    }
                ],
            });

            inlineAttachment.editors.codemirror4.attach(simplemde.codemirror, {
                uploadUrl: '/upload_image',
                uploadFieldName: "image",
                //urlText: "\n ![file]({filename})\n\n",
                extraParams: {
                    '_token': '{{ csrf_token() }}',
                },
                onFileUploadResponse: function (xhr) {
                    var result = JSON.parse(xhr.responseText),
                        filename = result[this.settings.jsonFieldName];

                    if (result && filename) {
                        var newValue;
                        if (typeof this.settings.urlText === 'function') {
                            newValue = this.settings.urlText.call(this, filename, result);
                        } else {
                            newValue = this.settings.urlText.replace(this.filenameTag, filename);
                        }
                        var text = this.editor.getValue().replace(this.lastValue, newValue);
                        this.editor.setValue(text);
                        this.settings.onFileUploaded.call(this, filename);
                    }
                    return false;
                }
            });


            // http://pandoc.org/README.html#pandocs-markdown
            var pandoc = [{
                filter: 'h1',
                replacement: function (content, node) {
                    var underline = Array(content.length + 1).join('=');
                    return '\n\n' + content + '\n' + underline + '\n\n';
                }
            },

                {
                    filter: 'h2',
                    replacement: function (content, node) {
                        var underline = Array(content.length + 1).join('-');
                        return '\n\n' + content + '\n' + underline + '\n\n';
                    }
                },

                {
                    filter: 'sup',
                    replacement: function (content) {
                        return '^' + content + '^';
                    }
                },

                {
                    filter: 'sub',
                    replacement: function (content) {
                        return '~' + content + '~';
                    }
                },

                {
                    filter: 'br',
                    replacement: function () {
                        return '\\\n';
                    }
                },

                {
                    filter: 'hr',
                    replacement: function () {
                        return '\n\n* * * * *\n\n';
                    }
                },

                {
                    filter: ['em', 'i', 'cite', 'var'],
                    replacement: function (content) {
                        return '*' + content + '*';
                    }
                },

                {
                    filter: function (node) {
                        var hasSiblings = node.previousSibling || node.nextSibling;
                        var isCodeBlock = node.parentNode.nodeName === 'PRE' && !hasSiblings;
                        var isCodeElem = node.nodeName === 'CODE' ||
                            node.nodeName === 'KBD' ||
                            node.nodeName === 'SAMP' ||
                            node.nodeName === 'TT';

                        return isCodeElem && !isCodeBlock;
                    },
                    replacement: function (content) {
                        return '`' + content + '`';
                    }
                },

                {
                    filter: function (node) {
                        return node.nodeName === 'A' && node.getAttribute('href');
                    },
                    replacement: function (content, node) {
                        var url = node.getAttribute('href');
                        var titlePart = node.title ? ' "' + node.title + '"' : '';
                        if (content === url) {
                            return '<' + url + '>';
                        } else if (url === ('mailto:' + content)) {
                            return '<' + content + '>';
                        } else {
                            return '[' + content + '](' + url + titlePart + ')';
                        }
                    }
                },

                {
                    filter: 'li',
                    replacement: function (content, node) {
                        content = content.replace(/^\s+/, '').replace(/\n/gm, '\n    ');
                        var prefix = '-   ';
                        var parent = node.parentNode;

                        if (/ol/i.test(parent.nodeName)) {
                            var index = Array.prototype.indexOf.call(parent.children, node) + 1;
                            prefix = index + '. ';
                            while (prefix.length < 4) {
                                prefix += ' ';
                            }
                        }

                        return prefix + content;
                    }
                }
            ];

            // http://pandoc.org/README.html#smart-punctuation
            var escape = function (str) {
                return str.replace(/[\u2018\u2019\u00b4]/g, "'")
                    .replace(/[\u201c\u201d\u2033]/g, '"')
                    .replace(/[\u2212\u2022\u00b7\u25aa]/g, '-')
                    .replace(/[\u2013\u2015]/g, '--')
                    .replace(/\u2014/g, '---')
                    .replace(/\u2026/g, '...')
                    .replace(/[ ]+\n/g, '\n')
                    .replace(/\s*\\\n/g, '\\\n')
                    .replace(/\s*\\\n\s*\\\n/g, '\n\n')
                    .replace(/\s*\\\n\n/g, '\n\n')
                    .replace(/\n-\n/g, '\n')
                    .replace(/\n\n\s*\\\n/g, '\n\n')
                    .replace(/\n\n\n*/g, '\n\n')
                    .replace(/[ ]+$/gm, '')
                    .replace(/^\s+|[\s\\]+$/g, '');
            };

            var convert = function (str) {
                return escape(toMarkdown(str, {
                    converters: pandoc,
                    gfm: true
                }));
            };

            // var pastebin = document.querySelector('#pastebin');

            // document.addEventListener('keydown', function(event) {
            //     if (event.ctrlKey || event.metaKey) {
            //         if (String.fromCharCode(event.which).toLowerCase() === 'v') {
            //             pastebin.innerHTML = '';
            //             pastebin.focus();
            //         }
            //     }
            // });

            // pastebin.addEventListener('paste', function() {
            //     setTimeout(function() {
            //         var html = pastebin.innerHTML;
            //         var markdown = convert(html);

            //         simplemde.codemirror.getDoc().setValue(simplemde.codemirror.getDoc().getValue() +"\n"+markdown);

            //         simplemde.codemirror.focus();
            //     }, 200);
            // });


        });

    </script>

@stop
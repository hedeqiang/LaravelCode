<!--评论-->
<div class="">
    <div class="card">
        <div class="card-header" style="margin-bottom: 0.3rem; margin-top: 0.5rem">
            @if(count($replies) <= 0)
                暂无回复
            @else
                {{count($replies)}}个回复
            @endif

        </div>
        <div class="card-body">
            {{--<div>--}}
                {{--<p><i class="fa fa-bookmark fa-3x" style="color: #309C04;" aria-hidden="true"></i>--}}
                    {{--<span class="align-top">最佳回复</span>--}}
                {{--</p>--}}
                {{--<hr/>--}}
            {{--</div>--}}
            @foreach ($replies as $index => $reply)
                <div class="replay-item">
                    <em class="aw-border-radius-5 aw-vote-count pull-left disabled" style="background: #F2F2F2;
    color: #999; min-width: 25px; text-align: center; border-radius: 5px;">1</em>
                    <div class="mod-head">
                        <p>
                            <a class="aw-user-name" href="#" data-id="66994">
                                {{ $reply->user->name }} </a>
                            <i style="color: #a5d300;" class="fa fa-vimeo" aria-hidden="true">
                                {{ $reply->created_at->diffForHumans() }}
                            </i>
                            <a class="pull-right" href="#" data-id="66994">
                                <img class="aw-user-img"
                                     src="{{ $reply->user->avatar }}"
                                     alt="{{ $reply->user->name }}">
                            </a>
                        </p>
                    </div>
                    <div class="replay-content">
                        {!! convert_markdown_to_html($reply->content) !!}
                    </div>

                    <div class="mod-footer" style="font-size: 12px;">
                        <i class="fa fa-thumbs-o-up"></i>
                        <a class="aw-add-comment" style="margin-left: 5px;color: #999; "
                           href="javascript:;">
                            <i class="fa fa-reply" aria-hidden="true"></i>
                        </a>
                        <hr/>
                    </div>
                </div>
            @endforeach

        </div>
    </div>
</div>

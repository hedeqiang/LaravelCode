@extends('layouts.app')
@section('title', "$post->title")
@section('description', "$post->title")
@section('keywords', "$post->title")

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/jquery.atwho.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/atom-one-dark.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/social-share.js/1.0.16/css/share.min.css">
@stop
@section('content')

    <div class="dg main">
        <div class="row">
            <div class="col-md-9 card">
                <div class="article border border-success">
                    <!--<h1 class="title">Hello World</h1>-->
                    <div class="alert alert-primary title text-center" role="alert">
                        {{$post->title}}
                    </div>
                    <div class="tag align-middle" style="text-align: center;	">
                        <div class="article-meta text-center">
                            <i class="fa fa-clock-o"></i> <abbr title="{{ $post->created_at }}"
                                                                class="timeago">{{$post->created_at->diffForHumans()}}</abbr>
                            <i class="fa fa-eye"></i> {{ $post->visits()->count() }}
                            <i class="fa fa-thumbs-o-up"></i> 19
                            <i class="fa fa-comments-o"></i> {{ $post->reply_count }}

                        </div>
                    </div>
                    <div class="replay-content" style="padding: 20px; padding-bottom: 20px;">
                        {!! convert_markdown_to_html($post->body) !!}

                        <div class="social-share"></div>
                    </div>

                    <div class="content-footer"
                         style="font-size: 12px; color: #999; padding-left: 20px; padding-bottom: 10px;">
                        <div class="aw-question-detail-meta">
                            <span class="aw-text-color-999">{{$post->created_at->diffForHumans()}}</span>
                            <a style="margin-right: 10px; color: rgb(153, 153, 153);" href="#replay">
                                <i class="fa fa-comment" style="margin-left: 10px; "></i>添加评论
                            </a>
                        </div>
                    </div>


                </div>

                @include('posts._reply_list',['replies' => $post->replies()->with('user')->get()])

                <!--评论回复框-->
                <div class="border-success" id="replay" style="margin-top: 1rem;">
                    <div class="alert alert-warning" role="alert">
                        支持 Markdown 格式, **粗体**、~~删除线~~、`单行代码`, 更多语法请见这里 <a class="alert-link"
                                                                           href="https://github.com/riku/Markdown-Syntax-CN/blob/master/syntax.md">Markdown</a>
                        语法
                    </div>

                    <div class="alert alert-info" role="alert">
                        <i class="fa fa-info" aria-hidden="true"></i>
                        请勿发布不友善或者负能量的内容。与人为善，比聪明更重要！
                    </div>
                    <form method="POST" action="{{ route('replies.store') }}"  accept-charset="UTF-8"
                          id="tweet-create-form" class="tweet-form reply-form">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <div class="form-group emoji-picker-container">
                        <textarea class="form-control" rows="4" placeholder="认真评论；支持 Markdown 格式"
                                  name="content" id="reply_content"></textarea>
                            <div class="form-group reply-post-submit replay-data">
                                <div class="pull-left meta">
                                    <a href="javascript:;" class="action popover-with-html"
                                       data-content="黏贴或拖拽图片至输入<br>框内皆可上传图片" data-original-title="" title="">
                                        <i class="fa fa-picture-o"></i>
                                    </a>
                                    <a href="javascript:;" class="action popover-with-html"
                                       data-content="支持除了 H1~H6 以外的<br>GitHub 兼容 Markdown" data-original-title=""
                                       title="">
                                        支持 MD
                                    </a>
                                </div>
                                <div class="pull-right">
                            <span class="help-inline meta" title="Or Command + Enter" style="margin-right:10px"><span
                                        id="word-count">0</span> / 180</span>
                                    <button class="btn btn-outline-success btn-sm " type="submit">
                                        <i class="fa fa-send"></i> 回复
                                    </button>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            @include('layouts.right',['posts' => visits(\App\Models\Post::class)->top(5)])

        </div>

        <i class="to-top fa fa-rocket fa-4x"></i>
    </div>

@stop

@section('scripts')
    {{--<script src="http://code.jquery.com/jquery.js"></script>--}}
    <script type="text/javascript"  src="{{ asset('js/jquery-1.11.0.min.js') }}"></script>


    <script type="text/javascript"  src="{{ asset('js/jquery.caret.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/jquery.atwho.min.js') }}"></script>

    <script type="text/javascript"  src="{{ asset('js/jquery.toTop.min.js') }}"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.12.0/highlight.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/social-share.js/1.0.16/js/jquery.share.min.js"></script>

    <script>
        $('#reply_content').atwho({
            at: "@",
            callbacks: {
                remoteFilter: function(query, callback) {
                    $.getJSON("/users_json", {q: query}, function(data) {
                        callback(data)
                    });
                }
            }
        })
    </script>

    <script>
        $('pre code').each(function (i, block) {
            hljs.highlightBlock(block);
        });
    </script>

    <script>
        $('.to-top').toTop({
            //options with default values
            autohide: true,  //boolean 'true' or 'false'
            offset: 420,     //numeric value (as pixels) for scrolling length from top to hide automatically
            speed: 500,      //numeric value (as mili-seconds) for duration
            right: 15,       //numeric value (as pixels) for position from right
            bottom: 30       //numeric value (as pixels) for position from bottom
        });
    </script>


@stop
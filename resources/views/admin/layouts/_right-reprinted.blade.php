<div class="col-md-3">
    <div class="card text-white bg-dark mb-3" style="max-width: 25rem; opacity: 0.8;">
        <div class="card-header">版权申明</div>
        <div class="card-body">

            <p class="card-text saying">
                本页内容均来自互联网优质内容，<strong class="font-weight-bold">版权并非本网站；</strong>
            </p>
            <p class="card-text saying">
                我们在原著允许转载的情况下进行转载。并且附上原著网站地址，文章链接等；
            </p>
            <p class="card-text saying">
                如果您觉得此举不妥，请告知我，我会第一时间进行下线处理；
            </p>
        </div>
    </div>


    <div class="list-group" style="max-width: 25rem;">
        <a href="#" class="list-group-item list-group-item-action active">
            热门排行
        </a>
        <a href="#" class="list-group-item list-group-item-action">Dapibus ac facilisis in</a>
        <a href="#" class="list-group-item list-group-item-action">Morbi leo risus</a>
        <a href="#" class="list-group-item list-group-item-action">Porta ac consectetur ac</a>
        <a href="#" class="list-group-item list-group-item-action disabled">Vestibulum at eros</a>
    </div>



    <div class="card text-white bg-info mb-3" style="max-width: 25rem; margin-top: 1rem;">
        <div class="card-header">标签云</div>
        <div class="card-body">
            <div class="tags">
                <span class="badge badge-primary">Primary</span>
                <span class="badge badge-secondary">Secondary</span>
                <span class="badge badge-success">Success</span>
                <span class="badge badge-danger">Danger</span>
                <span class="badge badge-warning">Warning</span>
                <span class="badge badge-info">Info</span>
                <span class="badge badge-light">Light</span>
                <span class="badge badge-dark">Dark</span>
                <span class="badge badge-primary">Primary</span>
                <span class="badge badge-secondary">Secondary</span>
                <span class="badge badge-success">Success</span>
                <span class="badge badge-danger">Danger</span>
                <span class="badge badge-warning">Warning</span>
                <span class="badge badge-info">Info</span>
                <span class="badge badge-light">Light</span>
                <span class="badge badge-dark">Dark</span>
                <span class="badge badge-primary">Primary</span>
                <span class="badge badge-secondary">Secondary</span>
                <span class="badge badge-success">Success</span>
                <span class="badge badge-danger">Danger</span>
                <span class="badge badge-warning">Warning</span>
                <span class="badge badge-info">Info</span>
                <span class="badge badge-light">Light</span>
                <span class="badge badge-dark">Dark</span>
            </div>

        </div>
    </div>
</div>
@extends('layouts.app')

@section('title')
    消息通知
@stop

@section('content')

    <div class="row {{ route_class() }}" >
        <div class="col-md-3">
            <div class="box mb-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link active " id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">全部</a>
                    <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">关注</a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">评论</a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false">点赞</a>
                </div>
            </div>
        </div>

        <div class="col-md-9">
            <div class="box box-flush">
                @if ($notifications->count())

                    <div class="notification-list">
                        @foreach ($notifications as $notification)
                            @include('notifications.types._' . snake_case(class_basename($notification->type)))
                        @endforeach
                        {!! $notifications->render() !!}
                    </div>

                @else
                    <div class="empty-block">没有消息通知！</div>
                @endif
            </div>

        </div>
    </div>

@stop
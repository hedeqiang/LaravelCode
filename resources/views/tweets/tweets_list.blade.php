
@foreach($tweets as $tweet)
<ul class="list-group  tweets-list">
    <li class="list-group-item media">
        <div class="avatar pull-left" style="margin-top:10px">
            <a href="{{ $tweet->user->profileLink }}">
                <img class="media-object avatar"
                     src="{{ $tweet->user->avatar }}"
                     style="width:48px;height:48px;">
            </a>
        </div>
        <div class="details">
            <div class="media-right">
                <a href="{{ $tweet->user->profileLink }}"
                   class="user-name"><strong>{{ $tweet->user->name }}</strong></a>
                <span class="meta" title="2018-07-22 00:41:39">{{ $tweet->created_at->diffForHumans() }}</span>
            </div>
            <div >
                <div class="media-body markdown-reply content-body text_reploy">
                    {!! convert_markdown_to_html($tweet->body) !!}
                </div>

                <span class="meta operation pull-right">
                                     <a href="#" class="action">
                                        <i class="fa fa-eye" style="font-size:14px;">
                                        </i>
                                        <span class="count">{{ $tweet->visits()->count() }}</span>
                                    </a>
                                    <a class="comment-vote action" id="reply-up-vote-14741" href="javascript:void(0);">
                                        <i class="fa fa-thumbs-o-up"></i>
                                        <span class="count vote-count">1</span>
                                    </a>

                                    <a href="#" class="action">
                                        <i class="fa fa-comments-o" style="font-size:14px;">
                                        </i>
                                        <span class="count">0</span>
                                    </a>
                                     <a href="javascript:void(0);" class="admin popover-with-html" data-toggle="modal">
                                        <i class="fa fa-flag"></i> 举报
                                     </a>
                                </span>
            </div>
        </div>
    </li>
</ul>

@endforeach
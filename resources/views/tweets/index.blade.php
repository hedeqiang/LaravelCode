@extends('layouts.app')
@section('title', '动弹')

@section('styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/emoji.css') }}">
@stop

@section('content')

    <div class="main">
        <div class="row">
            <div class="col-md-9 card">
                <form method="POST" action="{{ route('tweets.store') }}" accept-charset="UTF-8"
                      id="tweet-create-form" class="tweet-form">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="form-group emoji-picker-container">
                        <textarea class="form-control" rows="4" placeholder="分享美好的事物"
                                  name="body" data-emojiable="true" id="reply_content"></textarea>
                        <div class="form-group reply-post-submit data_info_s">
                            <div class="pull-left meta">
                                {{--<a href="javascript:;" class="action emoji-picker" data-type="picker">--}}
                                {{--<i class="fa fa-smile-o"></i>--}}
                                {{--</a>--}}
                                <a href="javascript:;" class="action popover-with-html"
                                   data-content="黏贴或拖拽图片至输入<br>框内皆可上传图片" data-original-title="" title="">
                                    <i class="fa fa-picture-o"></i>
                                </a>
                                <a href="javascript:;" class="action popover-with-html"
                                   data-content="支持除了 H1~H6 以外的<br>GitHub 兼容 Markdown" data-original-title="" title="">
                                    支持 MD
                                </a>
                            </div>
                            <div class="pull-right">
                            <span class="help-inline meta" title="Or Command + Enter" style="margin-right:10px"><span
                                        id="word-count">0</span> / 180</span>
                                <button class="btn btn-outline-success btn-sm " type="submit">

                                    <i class="fa fa-send"></i> 发布

                                </button>
                            </div>
                        </div>
                    </div>
                </form>

                @include('tweets.tweets_list')

                <div class="pull-right root-page">
                    {!! $tweets->links('vendor.pagination.bootstrap-4') !!}
                </div>

            </div>

            @include('tweets._hot-tweets')


        </div>


    </div>

@stop

@section('scripts')
    <script type="text/javascript"  src="{{ asset('js/config.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/util.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/jquery.emojiarea.js') }}"></script>
    <script type="text/javascript"  src="{{ asset('js/emoji-picker.js') }}"></script>

    <script>
        $(function () {
            // Initializes and creates emoji set from sprite sheet
            window.emojiPicker = new EmojiPicker({
                emojiable_selector: '[data-emojiable=true]',
                assetsPath: "{{ asset('images/emoji/') }}",
                popupButtonClasses: 'fa fa-smile-o'
            });
            // Finds all elements with `emojiable_selector` and converts them to rich emoji input fields
            // You may want to delay this step if you have dynamically created input fields that appear later in the loading process
            // It can be called as many times as necessary; previously converted input fields will not be converted again
            window.emojiPicker.discover();
        });
    </script>


@stop
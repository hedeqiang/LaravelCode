<div class="col-md-3">

    <div style="margin-top: 0.5rem;">
        <div class="panel panel-default corner-radius recommended-articles">
            <div class="panel-heading text-center">
                <h3 class="panel-title">热门动弹</h3>
            </div>
            <div class="panel-body">
                @foreach(visits(\App\Models\Tweet::class)->top(6) as $tweet)
                    <div class="media " style="display:table">
                        <div class="media-left">
                            {{--<a href="https://laravel-china.org/users/24868">--}}
                            <a href="#">
                                <img alt="64x64" class="media-object img-thumbnail avatar avatar-middle"
                                     data-src="holder.js/64x64" style="width: 26px;height: 26px;padding:0px;"
                                     src="{{ $tweet->user->avatar }}"
                                     data-holder-rendered="true">
                            </a>
                        </div>
                        <div class="media-body " style="display:table;width:100%;table-layout:fixed">
                            <div class="media-body markdown-reply content-body"
                                 style="display:block;width:100%;font-size: 14px;">
                                <a href="#" class="rm-link-color">{{ $tweet->user->name }}</a>：
                                <span class="rm-link-color clickable"
                                        onclick="javascript:Object.assign(document.createElement('a'), { href: '#'}).click();">
                                    {{ $tweet->body }}
                                </span>
                            </div>
                            <span class="meta operation">
                            <a href="#" class="action">{{ $tweet->created_at->diffForHumans() }}</a>
                                 <a class="comment-vote action" data-ajax="post" id="reply-up-vote-14647"
                                    href="javascript:void(0);"
                                    data-url="#" title="Vote Up">
                                 <i class="fa fa-thumbs-o-up"></i>
                                 <span class="count vote-count">0</span>
                                </a>
                           <a href="#" class="action">
                               <i class="fa fa-comments-o"></i>
                               <span class="count">0</span>
                           </a>
                        </span>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>


</div>